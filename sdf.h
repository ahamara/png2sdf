/*
 Copyright (C) 2014 Mikko Mononen (memon@inside.org)
 Copyright (C) 2009-2012 Stefan Gustavson (stefan.gustavson@gmail.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#ifndef SDF_H
#define SDF_H

// Sweep-and-update Euclidean distance transform of an antialised image for contour textures.
// Based on edtaa3func.c by Stefan Gustavson.
//
// White (255) pixels are treated as object pixels, zero pixels are treated as background.
// An attempt is made to treat antialiased edges correctly. The input image must have
// pixels in the range [0,255], and the antialiased image should be a box-filter
// sampling of the ideal, crisp edge. If the antialias region is more than 1 pixel wide,
// the result from this transform will be inaccurate.
// Pixels at image border are not calculated and are set to 0.
//
// The output distance field is encoded as bytes, where 0 = radius (outside) and 255 = -radius (inside).
// Input and output can be the same buffer.
//   out - Output of the distance transform, one byte per pixel.
//   outstride - Bytes per row on output image. 
//   radius - The radius of the distance field narrow band in pixels.
//   img - Input image, one byte per pixel.
//   width - Width if the image. 
//   height - Height if the image. 
//   stride - Bytes per row on input image. 
int sdfBuildDistanceField(unsigned char* out, int outstride, float radius,
						  const unsigned char* img, int width, int height, int stride);

// Same as distXform, but does not allocate any memory.
// The 'temp' array should be enough to fit width * height * sizeof(float) * 3 bytes.
void sdfBuildDistanceFieldNoAlloc(unsigned char* out, int outstride, float radius,
								  const unsigned char* img, int width, int height, int stride,
								  unsigned char* temp);

// This function converts the antialiased image where each pixel represents coverage (box-filter
// sampling of the ideal, crisp edge) to a distance field with narrow band radius of sqrt(2).
// This is the fastest way to turn antialised image to contour texture. This function is good
// if you don't need the distance field for effects (i.e. fat outline or dropshadow).
// Input and output buffers must be different.
//   out - Output of the distance transform, one byte per pixel.
//   outstride - Bytes per row on output image. 
//   img - Input image, one byte per pixel.
//   width - Width if the image. 
//   height - Height if the image. 
//   stride - Bytes per row on input image. 
void sdfCoverageToDistanceField(unsigned char* out, int outstride,
								const unsigned char* img, int width, int height, int stride);

#endif //SDF_H


