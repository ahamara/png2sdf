// png2sdf.cpp : Defines the entry point for the console application.
//

#include "lodepng.h"
#include "sdf.h"


 int main(int argc, char **argv)
{
	std::vector<unsigned char> image; //the raw pixels
	std::vector<unsigned char> imageOut; //the raw pixels
	unsigned width, height;


	if (argc != 4)
	{
		printf("Usage: %s in.png out.png 10.0\n", argv[0]);
	}
	else


	{
		//lodepng::load_file(image, argv[1]);

		lodepng::decode(image, width, height, argv[1]);

		unsigned char* sdfIn = new unsigned char[width*height];
		unsigned char* sdfOut = new  unsigned char[width*height];

		for (unsigned int i = 0; i < width*height; i++)
		{
			sdfIn[i] = image[i * 4];
		}

		sdfBuildDistanceField(sdfOut, width, (float)atof(argv[3]), sdfIn, width, height, width);

		for (unsigned int i = 0; i < width*height; i++)
		{
			image[i * 4 + 0] = sdfOut[i];
			image[i * 4 + 1] = sdfOut[i];
			image[i * 4 + 2] = sdfOut[i];
			image[i * 4 + 3] = 255;
		}

		lodepng::encode(argv[2], image, width, height);



	}
    return 0;
}

