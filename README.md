# png2sdf

Windows command line tool to convert png files to single distance field(SDF) contour textures.

## How to compile 

* Should compile with VS2015 later, might work with gcc. 
* Project files are for VS2017

## Usage

* Usage: png2sfd in.png out.png 10.0

* in.png: Input png. Should be black and white. Black is background color.
* out.png: Output png.
* 10.0: The radius of the distance field narrow band in pixels.

## Acknowledgments

* Uses: SDF creation is by: https://github.com/memononen/SDF
* Uses: PNG load and write is by: https://github.com/lvandeve/lodepng